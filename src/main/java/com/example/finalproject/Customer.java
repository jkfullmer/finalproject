package com.example.finalproject;

public class Customer {
    protected String name;
    protected String email;
    protected int age;
    public void customer(String name, String email, int age){
        this.name = name;
        this.email = email;
        this.age = age;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public int getAge() {
        return this.age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
