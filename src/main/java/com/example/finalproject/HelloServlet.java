package com.example.finalproject;

import java.io.*;
import java.sql.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + "Success!" + "</h1>");
        out.println("<p>" + "Your information has been recorded successfully." + "</p>");
        out.println("<p>" + "Welcome to the News4 You family!" + "</p>");
        out.println("</body></html>");
    }
    public static void insertProduct(Connection conn, Customer customer) throws SQLException {
        Customer newcustomer = new Customer();
        newcustomer.setName(username);
        newcustomer.setEmail(useremail);
        newcustomer.setAge(userage);

        String sql = "Insert into Product(Code, Name,Price) values (?,?,?)";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, customer.getName());
        pstm.setString(2, customer.getEmail());
        pstm.setInt(3, customer.getAge());

        pstm.executeUpdate();
    }
    public void destroy() {
    }
}